const express = require('express');
const fs = require('fs');
const path = require('path')

const app = express();

const rawdata = fs.readFileSync('testData/sw-categories.pretty.json');
const data = JSON.parse(rawdata);
const categories = Object.keys(data);
const moveFrom = "static/images/old";
const moveTo = "static/images/new";

// Loop through all the files in the temp directory
fs.readdir(moveFrom, (err, files) => {
    if(err) {
        console.error("Could not list the directory.", err);
        process.exit(1);
    }

    files.forEach((file, index) => {
        // Make one pass and make the file complete
        const fromPath = path.join(moveFrom, file);
        const toPath = path.join(moveTo, categories[index]+'.png');

        fs.stat(fromPath, (error, stat) => {
            if(error) {
                console.log("Error static file.", error);
                return;
            }

            if(stat.isFile()) {
                console.log("'%s' is a file.", fromPath);
            } else if (stat.isDirectory()) {
                console.log("'%s' is a directory.", fromPath);
            }

            fs.rename(fromPath, toPath, error => {
                if (error) {
                    console.error("File moving error.", error);
                } else {
                    console.log("Moved file '%s' to '%s'.", fromPath, toPath);
                }
            })
        })
    })
});

const server = app.listen(3400, function() {
    const host = server.address().address;
    const port = server.address().port;
    console.log('Example app listening at %s:%s', host, port);
});