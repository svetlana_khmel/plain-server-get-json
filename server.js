const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const axios = require('axios');


const app = express();

app.use(
    function (req, res, next) {
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Headers', 'Origin, X-Requestrd-With, Content-Type, Accept');
        next();
    }
);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.post('/api', function(req,res) {
    const json = JSON.stringify(req.body);
    fs.appendFile("testData/data.json", json , function (err) {
        if (err) throw err;
        console.log('The "data to append" was appended to file!');
    });

    const url = `http://127.0.0.1:5000//getCaregory/${json.currentUrl}`;

    axios.get(url)
        .then(response => {
            console.log('RESP: ', response.data);
            res.send({category: response.data});

        })
        .catch(error => {
            console.log(error);
            res.send(error);
        })
});

app.post('/prev', function(req,res) {
    console.log(req.body);      // your JSON
    const json = JSON.stringify(req.body);
    fs.writeFile("testData/prev.json", json , function (err) {
        if (err) throw err;
        console.log('The "data to append" was appended to file!');
    });
    res.send('Data has been stored successfully');
});

app.get('/getPrev', function(req,res) {
    fs.readFile('testData/prev.json', (err, data) => {
        if (err) throw err;

        if (Object.keys(data).length !== 0) {
            let prevData = JSON.parse(data);
            res.send(prevData)
        } else {
            res.send({"url":""})
        };
    });
});

app.get('http://127.0.0.1:5000/getCaregory/:url', function(req, res){
    console.log('http://127.0.0.1:5000/getCaregory/:url  ', req.params);
    res.send(req.params)
});

app.get('/getCaregory/:category', function(req, res){
    console.log('/getCaregory/:category', req.params);
    res.send(req.params)
});

app.get('/', function(req, res){
    res.send('hello world');
});

const server = app.listen(3400, function() {
    const host = server.address().address;
    const port = server.address().port;
    console.log('Example app listening at %s:%s', host, port);
});